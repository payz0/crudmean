var express 	= require("express");
var server  	= express();
var bodyParser 	= require("body-parser");

var router = require("./router.js");

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended:true}));
server.use(express.static(__dirname+"/lib"));
server.use("/", router);


server.listen(8080, function(){
	console.log("server running on 8080");
});