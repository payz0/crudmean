var app = angular.module("mean",[]);

angular.element(document).ready(function(){
	angular.bootstrap(document, ["mean"]);
});

app.controller("mycontrol", function($scope, $http){
	var getData = function(){
		$http.get("/data").success(function(data){
			$scope.semuaData = data;
			$scope.registrasi = "";
		})};

	getData();

	$scope.tambah = function(){
					$http.post("/data",$scope.registrasi).success(function(data){
						getData();
					});
					};
	$scope.del =	function(id){
					$http.delete("/data/"+id).success(function(){
						getData();
					})
					};

	$scope.edit = function(id){
					$http.post("/data/"+id).success(function(data){
						$scope.registrasi = data;
					});	

					$("#tambah").hide();
					$("#simpan").show();
				}
	$scope.simpan = function(){
					$http.put("/data/"+$scope.registrasi._id, $scope.registrasi).success(function(data){
						getData();
					});
					$("#tambah").show();
					$("#simpan").hide();
				}
	
});

	