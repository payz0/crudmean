var express 	= require("express");
var app 		= express();
var mongoose 	= require("./connection.js");
app.set("view engine", "ejs");
module.exports = app;

var tabel = mongoose.model("tabelReg", mongoose.Schema({
	nama : String,
	kelas : String
}));

app.get("/",function(req, res){
	res.render("home", {
		title : "Tutorial MEAN"
	});
});

app.post("/data", function(req, res){
	var data = req.body;
	console.log(data);
	tabel.create(data, function(err, docs){
		if(err) throw err;
		console.log(docs);
		res.json(docs);
	});
});

app.get("/data", function(req, res){
	tabel.find({}, function(err, docs){
		if(err) throw err;
		res.json(docs);
	});
});

app.delete("/data/:id", function(req, res){
	var id = req.params.id;
	tabel.findOneAndRemove({_id:id}, function(err, docs){
		if(err) throw err;
		console.log("deleted success");
		res.json(docs);
	});
});

app.post("/data/:id", function(req, res){
	var id = req.params.id;
	tabel.findOne({_id:id}, function(err, docs){
		res.json(docs);
	});
});

app.put("/data/:id",function(req, res){
	var id= req.params.id;
	tabel.findOneAndUpdate({_id:id},{$set:{nama:req.body.nama, kelas: req.body.kelas}},{new:true}, function(err, docs){
		if(err) throw err;
		console.log("data success edited");
		res.json(docs);
	});
});








